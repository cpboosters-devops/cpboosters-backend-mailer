Rails.application.routes.draw do
  resources :posts

  post "/contact", to: "contact_mails#contact"
end
